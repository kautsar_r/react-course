import React from 'react';
import ReactDOM from 'react-dom';

import './style.css';
import './tailwind.css';
import App from './App';

ReactDOM.render(<App />, document.getElementById('root'));
