import Todo from './components/Todo';

function App() {
	return (
		<div className="px-10 py-6">
			<h1 className="text-3xl font-bold">My Todos</h1>
			<Todo text="Belajar dasar React" />
			<Todo text="Belajar menengah React" />
			<Todo text="Belajar atas React" />
		</div>
	);
}

export default App;
