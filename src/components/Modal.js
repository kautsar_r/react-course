function Modal(props) {
	const cancelHandler = () => {
		props.onCancel();
	};

	const confirmHandler = () => {
		props.onConfirm();
	};

	return (
		<div className="absolute top-0 bottom-0 left-0 right-0 flex items-center justify-center">
			<div className="w-1/3 px-5 py-8 bg-white rounded shadow text-center z-50">
				<p className="text-2xl mb-8">Apakah kamu yakin?</p>
				<div className="flex justify-center">
					<button className="px-4 py-2 mx-4 border border-red-500 text-red-500 rounded" onClick={cancelHandler}>
						Cancel
					</button>
					<button className="px-4 py-2 mx-4 bg-blue-500 border border-blue-500 text-white rounded" onClick={confirmHandler}>
						Confirm
					</button>
				</div>
			</div>
		</div>
	);
}

export default Modal;
