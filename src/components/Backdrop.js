function Backdrop(props) {
	return <div className="absolute top-0 bottom-0 left-0 right-0 bg-black bg-opacity-80" onClick={props.onCancel} />;
}

export default Backdrop;
