import { useState } from 'react';

// Import components
import Modal from './Modal';
import Backdrop from './Backdrop';

function Todo(props) {
	const [isModalOpen, setModalOpen] = useState(false);

	const deleteHandler = () => {
		setModalOpen(true);
	};

	const closeModalHandler = () => {
		setModalOpen(false);
	};

	return (
		<div className="px-5 py-8 my-5 bg-white shadow-md rounded">
			<h2 className="text-2xl font-semibold">{props.text}</h2>
			<div className="flex justify-end mt-4">
				<button className="px-4 py-2 ml-auto bg-blue-600 text-white rounded" onClick={deleteHandler}>
					Delete
				</button>
			</div>
			{isModalOpen && <Modal onCancel={closeModalHandler} onConfirm={closeModalHandler} />}
			{isModalOpen && <Backdrop onCancel={closeModalHandler} />}
		</div>
	);
}

export default Todo;
